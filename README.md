# ffs

Fuzzing w/ Facebook's [fairseq](https://github.com/pytorch/fairseq).

## Install

```sh
pip3 install requirements.txt
```

Then build & install [sentencepiece](https://github.com/google/sentencepiece).

## Training a model

### Dataset

Put your files in `./dataset/<tag>/`.

### Preparing the dataset

```sh
./prepare-data.sh
```

### BPE + preprocessing

```sh
./preprocess.sh
```

### Train

```sh
train.sh
```

### Generate

```sh
# WARNING: output to STDOUT
./generate.sh <tag>
```

### Mutate file at byte X

TODO

## Where to get a dataset?

- Look at [oss-fuzz](https://github.com/google/oss-fuzz).
  Projects' scripts often download a batch of samples before starting the fuzzing campaign.
- `gsutil ls gs://fuzzbench-data/*20*/experiment-folders/**/corpus-archive-*tar.gz`
- You own files (:

## Credits

[babush](http://www.babush.me/)
