#!/usr/bin/env python3

import glob
import os
from pathlib import Path
import sys


def main(src_path='./dataset/', dst_path='./workdir/dataset.txt'):
    src_path = Path(src_path)
    dst_path = Path(dst_path)

    os.makedirs(dst_path.parent, exist_ok=True)

    with open(dst_path, 'w') as dst:
        for f in glob.glob(str(src_path / '*' / '*')):
            f = Path(f)
            tag = f.parent.name
            print(f'== {f} ==')
            with open(f, 'rb') as src:
                data = src.read()
                data = ''.join(chr(b + 0x4E00) for b in data)
                dst.write(f'<s> <{tag}> ')
                dst.write(data)
                dst.write(' ')
                dst.write('</s>\n')


if __name__ == '__main__':
    main(*sys.argv[1:])
