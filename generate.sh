#!/bin/bash

if [ -z "$1" ]
  then
    echo "Usage: $0 <file-extension>"
    exit 1
fi

TAG="$1"

mkseed() {
  seed=$(dd if=/dev/urandom count=4 bs=1 2>/dev/null \
      | od -t d \
      | head -n 1 \
      | sed -r 's/^.*\s+//g' \
      | tr '-' '+'
  )
}
mkseed

echo "<${TAG}>" \
  | fairseq-interactive                               \
      ./workdir/bin                                   \
      --task language_modeling                        \
      --path ./workdir/checkpoints/checkpoint_best.pt \
      --bpe sentencepiece                             \
      --remove-bpe                                    \
      --beam 1                                        \
      --nbest 1                                       \
      --seed $seed                                    \
      --sampling                                      \
  | grep 'H-0' | sed 's/^.*[>] //g' | python3 ./decoder.py
