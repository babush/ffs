#!/usr/bin/env python3

import sys

out = []
for line in sys.stdin:
    line = line.strip()
    for ch in line:
        ch = ord(ch) & 0xFF
        out.append(ch)
    break
sys.stdout.buffer.write(bytes(out))
