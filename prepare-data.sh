#!/bin/bash

python3 ./prepare-data.py

INPUT="./workdir/dataset.txt"
OUTPUT="./workdir/"

no_lines=$(wc -l "$INPUT" | cut -d" " -f1)

split -a 1 -l $[ $no_lines * 70 / 100 ] \
  <(shuf "$INPUT") \
  "$OUTPUT"

mv "$OUTPUT/a" "$OUTPUT/train.txt"
mv "$OUTPUT/b" "$OUTPUT/valid.txt"
