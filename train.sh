#!/bin/bash

fairseq-train --task language_modeling                 \
  ./workdir/bin                                        \
  --save-dir ./workdir/checkpoints/                    \
  --save-interval 10                                   \
  --arch transformer_lm                                \
  --share-decoder-input-output-embed                   \
  --dropout 0.1                                        \
  --optimizer adam                                     \
  --adam-betas '(0.9, 0.98)'                           \
  --weight-decay 0.01                                  \
  --clip-norm 0.0                                      \
  --lr 0.0005                                          \
  --lr-scheduler inverse_sqrt                          \
  --warmup-updates 4000                                \
  --warmup-init-lr 1e-07                               \
  --tokens-per-sample 512                              \
  --sample-break-mode none                             \
  --max-tokens 2048 --update-freq 16                   \
  --max-update 50000                                   \
  --max-epoch 500                                      \
  --bpe sentencepiece
