#!/bin/bash

VOCAB_SIZE=2000

WORKDIR=./workdir/
TRAIN=$WORKDIR/train.txt
VALID=$WORKDIR/valid.txt
BPE_CODE=$WORKDIR/bpe.code

TAGS=$(for d in ./dataset/*; do tag=$(basename "$d"); echo "<$tag>"; done | tr -d '\n' | sed 's/></>,</g')

if ! command -v spm_train &> /dev/null
then
  echo "Please install sentencepiece / spm_train (not just the python package)"
  exit 0
fi

echo 'learning bpe...'
spm_train --input="$TRAIN" --model_prefix="$WORKDIR/spm" --vocab_size=$VOCAB_SIZE --character_coverage=0.99995 --model_type=bpe --user_defined_symbols="$TAGS"
spm_encode --model="$WORKDIR/spm.model" --output_format=id < "$TRAIN" > $TRAIN.bpe
spm_encode --model="$WORKDIR/spm.model" --output_format=id < "$VALID" > $VALID.bpe

echo 'fairseq preprocess...'
fairseq-preprocess \
  --trainpref $TRAIN.bpe \
  --validpref $VALID.bpe \
  --destdir $WORKDIR/bin \
  --only-source \
  --task language_modeling \
  --workers 8
